import ProductList from "../components/product-list";
import { useSelector } from "react-redux";
import FormForBuy from "../components/form/form-for-buy";

export function Basket() {
  const products = useSelector((state) => state.merchandise.productsInBasket);
  return products.length ? (
    <>
      <FormForBuy />
      <ProductList products={products} page="basket" />
    </>
  ) : (
    "Корзина пуста"
  );
}
