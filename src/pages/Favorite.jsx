import ProductList from "../components/product-list";
import { useSelector } from "react-redux";

export function Favorite() {
  const products = useSelector(
    (state) => state.merchandise.productsInFavorite
  );

  return products.length ? (
    <>
      <ProductList
        products={products}
        page="favorite"
      />
    </>
  ) : (
    "Не додано жодного товару до обраного"
  );
}
