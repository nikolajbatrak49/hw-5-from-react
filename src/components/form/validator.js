import * as Yup from "yup";

export const validationSchema = Yup.object({
    firstName: Yup.string()
      .min(2, "Мінімум 2 символи")
      .max(20, "Максимум 20 символів")
      .required("Ім'я обов'язкове для вводу"),
    lastName: Yup.string()
      .min(2, "Мінімум 2 символи")
      .max(20, "Максимум 20 символів")
      .required("Прізвище обов'язкове для вводу"),
    age: Yup.number()
      .min(18, "Мінімальний вік 18 років")
      .max(120, "Максимальний вік 120 років")
      .required("Вік обов'язковий"),
    adress: Yup.string()
      .min(7, "Мінімум 7 символи")
      .max(80, "Максимум 80 символів")
      .required("Адреса обов'язкова для вводу"),
    phoneNumber: Yup.string()
      .required("Телефон обов'язковий для вводу"),
  });