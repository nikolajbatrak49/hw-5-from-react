import { useSelector, useDispatch } from "react-redux";
import { useFormik } from "formik";
import { PatternFormat } from "react-number-format";
import { validationSchema } from "./validator";
import { clearBasket } from "../../redux/actions/merchandise"
import styles from "../../styles/buyForm.module.scss";

export default function FormForBuy () {
  const dispatch = useDispatch();

  const productsIBasket = useSelector(
    (state) => state.merchandise.productsInBasket
  );

  const formik = useFormik({
    initialValues: {
      firstName: "",
      lastName: "",
      age: "",
      adress: "",
      phoneNumber: "",
    },
    validationSchema,
    onSubmit: (values) => {
      const newValues = {
        ...values,
        totalSum: totalSum(),
        products: productsIBasket,
      };
      console.log(newValues);
      dispatch(clearBasket());
      alert(`Вітаємо ${newValues.firstName} ${newValues.lastName}! Ви виручили нас на ${newValues.totalSum} UAH`);
    },
  });

  function totalSum() {
    return productsIBasket.reduce(
      (accumulator, product) => accumulator + product.price,
      0
    );
  }

  return (
    <form onSubmit={formik.handleSubmit} className={styles.FormForBuy}>
      <div className={styles.InputField}>
        <label htmlFor="firstName">Введіть ім'я</label>
        <input
          id="firstName"
          name="firstName"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.firstName}
        />
        {formik.errors.firstName && (
          <span className={styles.InputError}>{formik.errors.firstName}</span>
        )}
      </div>
      <div className={styles.InputField}>
        <label htmlFor="lastName">Введіть прізвище</label>
        <input
          id="lastName"
          name="lastName"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.lastName}
        />
        {formik.errors.lastName && (
          <span className={styles.InputError}>{formik.errors.lastName}</span>
        )}
      </div>
      <div className={styles.InputField}>
        <label htmlFor="age">Введіть вік</label>
        <input
          id="age"
          name="age"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.age}
        />
        {formik.errors.age && (
          <span className={styles.InputError}>{formik.errors.age}</span>
        )}
      </div>
      <div className={styles.InputField}>
        <label htmlFor="adress">Введіть адресу доставки</label>
        <input
          id="adress"
          name="adress"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.adress}
        />
        {formik.errors.adress && (
          <span className={styles.InputError}>{formik.errors.adress}</span>
        )}
      </div>
      <div className={styles.InputField}>
        <label htmlFor="phoneNumber">Введіть номер телефону</label>
        <PatternFormat
          format="+38 (0##) ### ## ##"
          allowEmptyFormatting
          mask="_"
          id="phoneNumber"
          name="phoneNumber"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.phoneNumber}
        />
        {formik.errors.phoneNumber && (
          <span className={styles.InputError}>{formik.errors.phoneNumber}</span>
        )}
      </div>
      <button type="submit">Купити все</button>
    </form>
  );
};

