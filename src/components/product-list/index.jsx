import PropTypes from "prop-types";

import styles from "../../styles/productList.module.scss";
import Product from "../product-item";

export default function ProductList({ products, page }) {
  return (
    <div className={styles.ProductList}>
      {products &&
        products.map((product) => (
          <Product product={product} key={product.article} page={page} />
        ))}
    </div>
  );
}

ProductList.propTypes = {
  products: PropTypes.array,
};
